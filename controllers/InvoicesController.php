<?php

namespace app\controllers;

use Yii;
use app\models\Invoices;
use app\models\InvoicesSearch;
use app\overrides\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InvoicesController implements the CRUD actions for Invoices model.
 */
class InvoicesController extends Controller
{
    /**
     * Lists all Invoices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Invoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Invoices();
        $model->scenario = Invoices::SCENARIO_CREATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Invoice has been created successfully'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
        Yii::$app->session->addFlash('error', Yii::t('app', 'Error while creating invoice'));
        return $this->redirect('index');
    }

    /**
     * Finds the Invoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAccept($id)
    {
        $userId = Yii::$app->user->id;
        $model = $this->findModel($id);
        if ($model->user_id_from != $userId && $model->changeStatus()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Invoice has been approved successfully'));
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Error while changing status'));
        }
        return $this->redirect('index');
    }
    
    public function actionDecline($id)
    {
        $userId = Yii::$app->user->id;
        $model = $this->findModel($id);
        if ($model->user_id_from != $userId && $model->changeStatus(Invoices::STATUS_DECLINE)) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Invoice has been declined successfully'));
        } else {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Error while changing status'));
        }
        return $this->redirect('index');
    }
}
