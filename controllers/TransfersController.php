<?php

namespace app\controllers;

use Yii;
use app\models\Transfers;
use app\models\TransfersSearch;
use app\overrides\controllers\Controller;
use yii\web\NotFoundHttpException;
use app\models\User;

/**
 * TransfersController implements the CRUD actions for Transfers model.
 */
class TransfersController extends Controller
{
    /**
     * Lists all Transfers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransfersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Transfers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transfers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Transfers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('app', 'Transfer has been created successfully'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
        Yii::$app->session->addFlash('error', Yii::t('app', 'Error while creating transfer'));
        return $this->redirect('index');
    }

    /**
     * Finds the Transfers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transfers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transfers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
