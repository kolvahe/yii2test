<?php

namespace app\components\traits;

use app\models\User;
use yii\helpers\ArrayHelper;

/**
 * trait SearchModelUsernameTrait
 * Use this trait to add sorting and filtering for custom field `username`
 *
 * @property string $username
 */
trait SearchModelUsernameTrait
{
    public $username;

    public function addUsernameProcessing($dataProvider)
    {
        if ($this->username) {
            $this->addUsernameFilter($dataProvider);
        }
        $this->addUsernameSort($dataProvider);
    }

    protected function addUsernameFilter($dataProvider)
    {
        $dataProvider->query->andWhere([
            'user_id_to' => ArrayHelper::getColumn(
                User::find()->andFilterWhere([
                    'like', 'username', $this->username
                ])
                ->all(),
                'id'
            ),
            'user_id_from' => ArrayHelper::getColumn(
                User::find()->andFilterWhere([
                    'like', 'username', $this->username
                ])
                ->all(),
                'id'
            ),
        ]);
    }

    protected function addUsernameSort($dataProvider)
    {
        $dataProvider->sort->attributes['username'] = [
            'asc' => [
                User::tableName() . '.username' => SORT_ASC,
            ],
            'desc' => [
                User::tableName() . '.username' => SORT_DESC,
            ],
            'label' => 'Username',
            'default' => SORT_ASC
        ];
    }
}