<?php

namespace app\components\validators;

use Yii;
use yii\validators\Validator;

class SelfSendValidator extends Validator
{
    public $user;
    
    public function init()
    {
        parent::init();
        $this->message = Yii::t('app', 'You can`t send money to yourself');
        $this->user = Yii::$app->user->identity;
    }

    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        if ($this->user->username == $value) {
            $model->addError($attribute, 'asd');
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $username = $this->user->username;
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS
if (value == "$username") {
    messages.push($message);
}
JS;
    }
}