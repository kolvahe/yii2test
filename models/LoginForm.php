<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $rememberMe = true;

    private $_user;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username', 'string', 'max' => 32],
            // username required
            [['username'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }
    
    public function signup()
    {
        $this->_user = new User();
        $this->_user->scenario = User::SCENARIO_SIGNUP;
        $this->_user->username = $this->username;
        $this->_user->generateAuthKey();
        if (!$this->_user->save()) {
            Yii::error('Error while registering new user', 'User');
            return;
        }
        return $this->_user;
    }

    /**
     * Finds user by [[username]]
     * Register otherwise
     * @return User
     */
    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = User::findByUsername($this->username);
            if (!$this->_user) {
                $this->signup();
            }
        }
        return $this->_user;
    }
}
