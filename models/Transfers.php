<?php

namespace app\models;

use Yii;
use app\overrides\models\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use app\components\validators\SelfSendValidator;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "{{%transfers}}".
 *
 * @property integer $id
 * @property integer $user_id_from
 * @property integer $user_id_to
 * @property string $price
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Transfers extends ActiveRecord
{
    public $username;
    public $isIncoming;
    
    const LABEL_INCOMING = 0;
    const LABEL_OUTCOMING = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transfers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'string', 'max' => 32],
            ['username', SelfSendValidator::className()],
            [['username', 'price'], 'required'],
            [['user_id_from', 'user_id_to', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            ['price', 'compare', 'compareValue' => 0, 'operator' => '>'],
            ['isIncoming', 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return array_merge(
            parent::behaviors(),
            [
                [
                    'class' => BlameableBehavior::className(),
                    'createdByAttribute' => 'user_id_from',
                    'updatedByAttribute' => false,
                ],
                'user_id_to' => [
                    'class' => AttributeBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => 'user_id_to',
                    ],
                    'value' => function() {
                        $loginForm = new LoginForm(['username' => $this->username]);
                        $user = $loginForm->getUser();
                        return $user->id;
                    },
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'user_id_from' => Yii::t('app', 'User Id From'),
            'user_id_to' => Yii::t('app', 'User Id To'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSender()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_to']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $user = User::findById($this->user_id_to);
            if ($this->makeTransfer($user)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 
     * @return boolean
     */
    protected function makeTransfer($user)
    {
        if (
            $user
            && $user->increaseBalance($this->price)
            && $this->userSender->decreaseBalance($this->price)
        ) {
            return true;
        }
        return false;
    }

    public static function getIncOutTexts()
    {
        return [
            self::LABEL_INCOMING => 'Incoming',
            self::LABEL_OUTCOMING => 'Outcoming',
        ];
    }
}
