<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transfers;
use app\components\traits\SearchModelUsernameTrait;

/**
 * TransfersSearch represents the model behind the search form about `app\models\Transfers`.
 */
class TransfersSearch extends Transfers
{
    use SearchModelUsernameTrait;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'string', 'max' => 32],
            [['id', 'user_id_from', 'user_id_to', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            ['isIncoming', 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transfers::find();
        $query->joinWith('userRecipient');
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if ($this->isIncoming != '') {
            if ($this->isIncoming) {
                $query->andWhere(['user_id_from' => Yii::$app->user->identity->id]);
            } else {
                $query->andWhere(['user_id_to' => Yii::$app->user->identity->id]);
            }
        } else {
            $query->andWhere([
                'or',
                ['user_id_to' => Yii::$app->user->identity->id],
                ['user_id_from' => Yii::$app->user->identity->id]
            ]);
        }
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            Transfers::tableName().'.id' => $this->id,
            Transfers::tableName().'.user_id_from' => $this->user_id_from,
            Transfers::tableName().'.user_id_to' => $this->user_id_to,
            Transfers::tableName().'.price' => $this->price,
            Transfers::tableName().'.status' => $this->status,
            Transfers::tableName().'.created_at' => $this->created_at,
            Transfers::tableName().'.updated_at' => $this->updated_at,
        ]);
        
        $this->addUsernameProcessing($dataProvider);

        return $dataProvider;
    }
}
