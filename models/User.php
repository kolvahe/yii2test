<?php

namespace app\models;

use Yii;
use app\overrides\models\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_SIGNUP = 'signup';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                ['username', 'string'],
                ['username', 'required'],
                ['username', 'unique', 'on' => self::SCENARIO_SIGNUP],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_SUCCESS]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }
    
    public static function findById($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'balance' => Yii::t('app', 'Balance'),
        ];
    }
    
    public function increaseBalance($price)
    {
        $this->balance += $price;
        if (!$this->save()) {
            Yii::error($this->getErrors(), 'User Balance');
            return false;
        }
        return true;
    }
    
    public function decreaseBalance($price)
    {
        $this->balance -= $price;
        if (!$this->save()) {
            Yii::error($this->getErrors(), 'User Balance');
            return false;
        }
        return true;
    }
}
