<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('log in/sign up user');
$I->amOnPage('/site/login');
$I->fillField('Username','TestAcceptUser');
$I->click('Login');
$I->lookForwardTo('Logout (TestAcceptUser)');