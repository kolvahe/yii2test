-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_from` int(11) NOT NULL,
  `user_id_to` int(11) NOT NULL,
  `price` decimal(8,2) DEFAULT '0.00',
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_invoices_user` (`user_id_from`),
  CONSTRAINT `FK_invoices_user` FOREIGN KEY (`user_id_from`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `invoices` (`id`, `user_id_from`, `user_id_to`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1,	1,	2,	1.00,	10,	1460674825,	1460676062),
(2,	3,	2,	1.00,	10,	1460676283,	1460676349),
(3,	2,	1,	50.00,	10,	1460676431,	1460676455),
(4,	4,	5,	10.00,	10,	1460710378,	1460710405);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1460674725),
('m160414_154053_user',	1460674805),
('m160414_164907_transfers',	1460674806),
('m160414_164924_invoices',	1460674806);

DROP TABLE IF EXISTS `transfers`;
CREATE TABLE `transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_from` int(11) NOT NULL,
  `user_id_to` int(11) NOT NULL,
  `price` decimal(8,2) DEFAULT '0.00',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_transfers_user` (`user_id_from`),
  CONSTRAINT `FK_transfers_user` FOREIGN KEY (`user_id_from`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `transfers` (`id`, `user_id_from`, `user_id_to`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1,	1,	2,	1.00,	10,	1460674842,	1460674842),
(2,	2,	3,	2.00,	10,	1460676229,	1460676229),
(3,	2,	3,	22.00,	10,	1460676410,	1460676410),
(4,	4,	5,	10.00,	10,	1460710323,	1460710323);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(8,2) DEFAULT '0.00',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `balance`, `status`, `created_at`, `updated_at`) VALUES
(1,	'qwe',	'ii_CjbH_zO00jJmVEa2DXXucSXbwtN7F',	-50.00,	10,	1460674815,	1460676455),
(2,	'asd',	'-OJUncwFBvfRe0O-GURPZILzCtB7QbDl',	25.00,	10,	1460674825,	1460676455),
(3,	'zzz',	'vD4alNrITo0m9J1iawEEum4VkdKS81qb',	25.00,	10,	1460676229,	1460676410),
(4,	'ttt',	'8GXmxgHBywzFxUqq6uJQqkcTeayqxGR4',	0.00,	10,	1460710310,	1460710405),
(5,	'ddd',	'9jVAmC3AbwmCWyXduYpHzy9_n5KKRssB',	0.00,	10,	1460710323,	1460710405);

-- 2016-04-15 09:53:45
