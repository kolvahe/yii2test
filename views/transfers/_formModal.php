<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Transfers */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Modal::begin([
    'id' => 'transferModal',
    'header' => '<h2>Transfer money</h2>',
]);?>

<div class="transfers-form">
    <?php $form = ActiveForm::begin([
        'action' => ['create'],
    ]); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Transfer'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php Modal::end();?>