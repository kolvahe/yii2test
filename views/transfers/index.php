<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Transfers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransfersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transfers');
$this->params['breadcrumbs'][] = $this->title;

$userId = Yii::$app->user->id;
?>
<div class="transfers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button(Yii::t('app', 'Transfer'), [
            'data' => [
                'toggle' => 'modal',
                'target' => '#transferModal',
            ],
            'class' => 'btn btn-success'
        ]) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'isIncoming',
                'format' => 'raw',
                'filter' => Transfers::getIncOutTexts(),
                'value' => function ($model) use ($userId) {
                    if ($model->userRecipient->id != $userId) {
                        $icon = '<span class="glyphicon glyphicon-arrow-up text-success"></span>';
                    } else {
                        $icon = '<span class="glyphicon glyphicon-arrow-down text-info"></span>';
                    }
                    return $icon;
                },
            ],
            [
                'attribute' => 'username',
                'value' => function ($model) use ($userId) {
                    $sender = $model->userRecipient;
                    return $sender->id != $userId ? $sender->username : $model->userSender->username;
                },
            ],
            'price',
            [
                'attribute' => 'status',
                'filter' => Transfers::getStatusTexts(),
                'value' => 'statusText',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
    
    <?= $this->render('_formModal', ['model' => new Transfers]) ?>
</div>
