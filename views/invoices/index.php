<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Invoices;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;

$userId = Yii::$app->user->id;
?>
<div class="invoices-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button(Yii::t('app', 'Make invoice'), [
            'data' => [
                'toggle' => 'modal',
                'target' => '#invoiceModal',
            ],
            'class' => 'btn btn-success'
        ]) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'isIncoming',
                'format' => 'raw',
                'filter' => Invoices::getIncOutTexts(),
                'value' => function ($model) use ($userId) {
                    if ($model->userSender->id == $userId) {
                        $icon = '<span class="glyphicon glyphicon-arrow-up text-success"></span>';
                    } else {
                        $icon = '<span class="glyphicon glyphicon-arrow-down text-info"></span>';
                    }
                    return $icon;
                },
            ],
            [
                'attribute' => 'username',
                'value' => function ($model) use ($userId) {
                    $sender = $model->userSender;
                    return $sender->id != $userId ? $sender->username : $model->userRecipient->username;
                },
            ],
            'price',
            [
                'attribute' => 'status',
                'filter' => Invoices::getStatusTexts(),
                'value' => 'statusText',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {accept} {decline}',
                'visibleButtons' => [
                    'accept' => function ($model, $key, $index) use ($userId) {
                        return $model->userSender->id != $userId && $model->status === Invoices::STATUS_PENDING;
                    },
                    'decline' => function ($model, $key, $index) use ($userId) {
                        return $model->userSender->id != $userId && $model->status === Invoices::STATUS_PENDING;
                    },
                ],
                'buttons' => [
                    'accept' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'Accept'),
                            'aria-label' => Yii::t('yii', 'Accept'),
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
                    },
                    'decline' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'decline'),
                            'aria-label' => Yii::t('yii', 'decline'),
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, $options);
                    },
                ]
            ],
        ],
    ]); ?>
    <?= $this->render('_formModal', ['model' => new Invoices]) ?>
</div>
