<?php

use yii\db\Migration;
use app\models\Invoices;

class m160414_164924_invoices extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%invoices}}', [
            'id' => $this->primaryKey(),
            'user_id_from' => $this->integer()->notNull(),
            'user_id_to' => $this->integer()->notNull(),
            'price' => $this->money(8, 2)->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(Invoices::STATUS_PENDING),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('FK_invoices_user', '{{%invoices}}', 'user_id_from', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('FK_invoices_user', '{{%invoices}}');
        $this->dropTable('{{%invoices}}');
    }
}
