<?php
namespace app\overrides\models;

use Yii;
use yii\db\ActiveRecord as BaseActiveRecord;
use yii\behaviors\TimestampBehavior;

class ActiveRecord extends BaseActiveRecord
{
    const STATUS_DECLINE = 0;
    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 10;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timeStampBehavior' => TimestampBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     * @return ActiveRecordQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActiveRecordQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public static function findAll($condition = null)
    {
        if (!empty($condition)) {
            return parent::findAll($condition);
        }

        return self::find()->all();
    }

    public static function getStatusTexts()
    {
        return [
            self::STATUS_SUCCESS => 'Success',
            self::STATUS_DECLINE => 'Decline',
            self::STATUS_PENDING => 'Pending',
        ];
    }

    public function getStatusText()
    {
        $statuses = $this->getStatusTexts();
        return (!empty($statuses[$this->status])) ? $statuses[$this->status] : '';
    }
    
    public function changeStatus($status = self::STATUS_SUCCESS)
    {
        if ($this->status != $status) {
            $this->status = $status;
            return $this->save();
        }
        return false;
    }
}
